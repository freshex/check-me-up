<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 2:13 PM
 */

require "../header.php"; ?>

<form id="generated">
    <input id="autofield0" type="text" name="conditions[]" onkeyup="showResult(this.value)"/><br />
</form>

    <div id="ajax" class="center"></div>

<!-- Blank field - used for copying -->
<div id="defaultfield" style="display: none"><input id="new" type="text" name="conditions[]" onkeyup="showResult(this.value)"/><br /></div>

<!-- AJAX & Popup code -->
<script src="/res/scripts/autocomplete-popup.js"></script>

<?php require "../footer.php"; ?>