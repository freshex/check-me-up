<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 11:32 AM
 */

/*
 * WHAT TO DO WITH THIS FILE
 *
 * This file is an example config file.
 * It should be renamed to "config.php" and filled in with
 * the correct values for your system.
 */

$sql = [
    "host" => "HOST OR DOMAIN NAME",
    "database" => "DATABASE NAME",
    "user" => "USERNAME",
    "password" => "PASSWORD",
];