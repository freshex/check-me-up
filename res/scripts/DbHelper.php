<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 12:07 PM
 */

require_once "User.php";
require_once "TokenGenerator.php";
require_once "Hospital.php";
require_once "CookieHelper.php";
require_once "Suggestion.php";

class DbHelper{
    //database vars
    private $domain = "";
    private $database = "";
    private $username = "";
    private $password = "";

    private $db = null;

    //static table names
    static $TABLE_CONDITIONS = "conditions";
    static $TABLE_USERS = "users";
    static $TABLE_TOKENS = "tokens";
    static $TABLE_HOSPITALS = "hospitals";
    static $TABLE_SUGGESTIONS = "suggestions";

    public function __construct(){
        //get required variables outside of source control
        require("config.php");

        //update variables to be included in class scope
        $this->domain = $sql['host'];
        $this->database = $sql['database'];
        $this->username = $sql['user'];
        $this->password = $sql['password'];

        //initiate the connection
        $this->connectToDatabase();
    }

    private function error(){
        die("Oops! Something went wrong. Please try again.");
    }

    private function connectToDatabase(){
        $this->db = new mysqli($this->domain, $this->username, $this->password, $this->database);
    }

    public function searchConditions($str){
        $TABLE_CONDITIONS = self::$TABLE_CONDITIONS;
        $searchStr = strtoupper($str);

        $sql = " SELECT * FROM $TABLE_CONDITIONS WHERE Name LIKE '$searchStr%' ";

        $result = $this->db->query($sql);
        if($result){
            $conditions = [];

            while ($row = $result->fetch_assoc()) {
                //add value to list
                $value = $row["Name"];
                $conditions[] = $value;
            }
            return $conditions;
        }else{
            die("No results");
        }
    }

    public function registerUser(User $userObj)
    {
        $TABLE_USERS = self::$TABLE_USERS;

        //get vars from User
        $password = $userObj->getPassword();
        $name = $userObj->getName();
        $email = $userObj->getEmail();

        //escape characters
        $password = $this->db->real_escape_string($password);
        $name = $this->db->real_escape_string($name);
        $email = $this->db->real_escape_string($email);

        if ($this->userExists($email) == false) {
            $sql = "INSERT INTO $TABLE_USERS (Password, FullName, Email) VALUES ( '$password', '$name', '$email' )";

            $this->db->query($sql) or die('Failed to insert User object into database.');
            return true;
        } else {
            echo "Username already in use";
            return false;
        }
    }

    public function userExists($email){
        $TABLE_USERS = self::$TABLE_USERS;

        $email = $this->db->real_escape_string($email);

        $sql = "SELECT * FROM $TABLE_USERS WHERE Email = '$email' ";
        
        $result = $this->db->query($sql) or die('Failed to process SELECT Users query.');

        if ($result->num_rows >= 1) {
            return true;
        }else{
            return false;
        }
    }

    public function selectUser($email, $rawPassword){
        $TABLE_USERS = self::$TABLE_USERS;

        $email = $this->db->real_escape_string($email);
        $rawPassword = $this->db->real_escape_string($rawPassword);

        $sql = "SELECT * FROM $TABLE_USERS WHERE Email = '$email' ";
        $result = $this->db->query($sql) or die('Failed to process SELECT Users query.');
        
        if ($result->num_rows == 1) {

            $row = $result->fetch_assoc();
            $user = new User($row['Email'], $row['Password'], $row['FullName']);

            $user->setUserId($row['Id']);
            
            if (CryptHelper::verify($rawPassword, $user->getPassword())) {
                
                return $user;
            } else {
                return false;
            }

        } else {
            return null;
        }
    }

    public function registerToken($email){
        $TABLE_TOKENS = self::$TABLE_TOKENS;

        $email = $this->db->real_escape_string($email);

        $sql = "DELETE FROM $TABLE_TOKENS WHERE Email = '$email' ";
        $this->db->query($sql);

        $token = TokenGenerator::generate();

        $sql = "INSERT INTO $TABLE_TOKENS (Username, Token) VALUES ( '$email', '$token' )";
        
        $this->db->query($sql) or die('Failed to registerToken.');

        return $token;
    }

    public function selectHospital($id){
        $TABLE_HOSPITALS = self::$TABLE_HOSPITALS;

        $id = $this->db->real_escape_string($id);
        $sql = "SELECT * FROM $TABLE_HOSPITALS WHERE Id = '$id'";
        $results = $this->db->query($sql);

        if($results){
            $row = $results->fetch_assoc();
            $name = $row['Name'];
            $phone = $row['Phone'];
            $address = $row['Location'];
            $servicesCSV = $row['Services'];
            $type = $row['Type'];

            $hospital = new Hospital($name,$phone,$address);
            $hospital->setServicesFromCSV($servicesCSV);
            $hospital->setType($type);
            return $hospital;
        }

        return null;
    }

    public function searchHospitalByServices($serviceArray){
        $TABLE_HOSPITALS = self::$TABLE_HOSPITALS;

        $hospitalList = [];

        //base sql statement
        $sql = "SELECT * FROM $TABLE_HOSPITALS WHERE Services LIKE '%$serviceArray[0]%'";
        //build dynamically based on service array
        $serviceLength = count($serviceArray);
        for($i = 1; $i < $serviceLength; $i++){
            $sql = $sql . " OR Services LIKE '%" . $serviceArray[$i] . "%'";
        }

        //sql string has now been build, execute query
        $results = $this->db->query($sql);
        if($results){
            while ($row = $results->fetch_assoc()) {
                //get hospital info
                $row = $results->fetch_assoc();
                $name = $row['Name'];
                $phone = $row['Phone'];
                $address = $row['Location'];
                $servicesCSV = $row['Services'];
                $type = $row['Type'];

                //create new Hospital
                $hospital = new Hospital($name, $phone, $address);
                $hospital->setServicesFromCSV($servicesCSV);
                $hospital->setType($type);

                //add the hospital to the list
                $hospitalList[] = $hospital;
            }
        }

        return $hospitalList;
    }

    public function updateUser(User $user){
        $TABLE_USERS = self::$TABLE_USERS;

        $id = $user->getUserId();
        $name = $user->getName();
        $email = $user->getEmail();
        $password = $user->getPassword();
        $phone = $user->getPhone();
        $DOB = $user->getDOB();
        $sex = $user->getSex();
        $conditions = serialize($user->getConditionsArray());
        $insurance = $user->getInsurance();
        $plan = $user->getPlan();

        $sql = "UPDATE $TABLE_USERS SET FullName = '$name', Email = '$email', Password = '$password',
          Phone = '$phone', DOB = '$DOB', Gender = '$sex', Conditions = '$conditions',
          Insurance = '$insurance', Plan = '$plan' WHERE Id = '$id' ";

        //submit update request
        $this->db->query($sql) or die("Failed to update user.") ;
    }

    public function selectCurrentUser(){
        $TABLE_USERS = self::$TABLE_USERS;

        $token = CookieHelper::getToken();
        $email = CookieHelper::getUser();

        //TODO: Verify token
        $sql = "SELECT * FROM $TABLE_USERS WHERE Email = '$email' ";
        $result = $this->db->query($sql) or die('Failed to process SELECT Users query.');

        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $user = new User($row['Email'], $row['Password'], $row['FullName']);
            $user->setUserId($row['Id']);
            $user->setPhone($row['Phone']);
            $user->setDob($row['DOB']);
            $user->setSex($row['Gender']);
            $user->setInsurance($row['Insurance']);
            $user->setPlan($row['Plan']);
            $serializedConditions = $row['Conditions'];
            $conditions = unserialize($serializedConditions);
            $user->setConditionsArray($conditions);

            return $user;
        } else {
            return null;
        }
    }

    public function getSuggestions(){
        $TABLE_SUGGESTIONS = self::$TABLE_SUGGESTIONS;

        $suggestionList = [];

        $sql = "SELECT * FROM $TABLE_SUGGESTIONS";
        
        $results = $this->db->query($sql);

        while($row = $results->fetch_assoc()){
            $name = $row['Name'];
            $minAge = $row['Min'];
            $maxAge = $row['Max'];
            $frequency = $row['Frequency'];
            $notes = $row['Notes'];
            $sex = $row['Gender'];
            $id = $row['Id'];

            $suggestion = new Suggestion($name,$sex);
            $suggestion->setMinAge($minAge);
            $suggestion->setMaxAge($maxAge);
            $suggestion->setFrequency($frequency);
            $suggestion->setNotes($notes);
            $suggestion->setId($id);

            $suggestionList[] = $suggestion;
        }

        return $suggestionList;
    }

    public function getSuggestionsByUser(User $user){
        $TABLE_SUGGESTIONS = self::$TABLE_SUGGESTIONS;

        $suggestionList = [];

        $age = $user->getAge();
        $sex = $user->getSex();

        $sql = "SELECT * FROM $TABLE_SUGGESTIONS WHERE Min <= $age AND Max >= $age AND Gender = '$sex' ";
        
        $results = $this->db->query($sql);

        while($row = $results->fetch_assoc()){
            $name = $row['Name'];
            $minAge = $row['Min'];
            $maxAge = $row['Max'];
            $frequency = $row['Frequency'];
            $notes = $row['Notes'];
            $sex = $row['Gender'];
            $id = $row['Id'];

            $suggestion = new Suggestion($name,$sex);
            $suggestion->setMinAge($minAge);
            $suggestion->setMaxAge($maxAge);
            $suggestion->setFrequency($frequency);
            $suggestion->setNotes($notes);
            $suggestion->setId($id);

            $suggestionList[] = $suggestion;
        }

        return $suggestionList;
    }
}