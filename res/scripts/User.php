<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 5:07 PM
 */

require_once "CryptHelper.php";
require_once "DbHelper.php";

class User implements JsonSerializable{
    static $COOKIE_KEY = "user";

    private $userId = null;
    private $password = null;
    private $fullName = null;
    private $email = null;
    private $phone = null;
    private $dob = null;
    private $sex = null;
    private $insurance = null;
    private $plan = null;
    private $conditionsArray = null;

    public function __construct($email, $password, $fullName){
        $this->setEmail($email);
        $this->setPassword($password);
        $this->setName($fullName);
    }

    public function jsonSerialize(){
        $result = get_object_vars($this);
        return json_encode($result);
    }

    //setters
    public function setUserId($userId){
        $this->userId = $userId;
    }
    public function setPassword($password){
        $this->password = $password;
    }
    public function setName($fullName){
        $this->fullName = $fullName;
    }
    public function setEmail($email){
        $this->email = $email;
    }
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    public function setDob($dob)
    {
        $this->dob = $dob;
    }
    public function setSex($sex)
    {
        $this->sex = $sex;
    }
    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;
    }
    public function setPlan($plan)
    {
        $this->plan = $plan;
    }
    public function setConditionsArray($conditionsArray)
    {
        $this->conditionsArray = $conditionsArray;
    }

    //getters
    public function getUserId(){
        return $this->userId;
    }
    public function getPassword(){
        return $this->password;
    }
    public function getName(){
        return $this->fullName;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getPhone(){
        return $this->phone;
    }
    public function getDob(){
        return $this->dob;
    }
    public function getSex(){
        return $this->sex;
    }
    public function getInsurance(){
        return $this->insurance;
    }
    public function getPlan(){
        return $this->plan;
    }
    public function getConditionsArray(){
        return $this->conditionsArray;
    }

    public function generatePassword($plainText){
        //generate password hash
        $this->setPassword( CryptHelper::generate($plainText) );
    }

    public function register(){
        //if all fields exist...
        if( $this->getPassword() && $this->getName() && $this->getEmail() ){
            //Register the user w/ the db
            $dbHelper = new DbHelper();
            return $dbHelper->registerUser($this);
        }
    }

    public function update(){
        if( $this->getPassword() && $this->getName() && $this->getEmail() ){
            $dbHelper = new DbHelper();
            $dbHelper->updateUser($this);
        }
    }

    public function getAge(){
        $date = DateTime::createFromFormat("Y-m-d", $this->getDob());
        $birthYear = $date->format("Y");
        $nowYear = date("Y");

        return $nowYear - $birthYear;
    }
}