<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 4:50 PM
 */

class TokenGenerator {
    static $COOKIE_KEY = "token";

    static public function generate(){
        $token = bin2hex(openssl_random_pseudo_bytes(16));

        return $token;
    }
}