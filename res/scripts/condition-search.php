<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 12:03 PM
 */

require "DbHelper.php";
require "UI.php";

$query = $_GET['q'];

$dbHelper = new DbHelper();
$results = $dbHelper->searchConditions($query);

UI::createPopup($results);