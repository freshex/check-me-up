<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 2:04 PM
 */

class UI {
    static function createPopup($array){
        echo " <div class='autocompletedialog'> ";
        echo "<ul>";
        $length = count($array);

        for($i = 0; $i < $length && $i < 5; $i++){
            echo " <li><a onclick='addOption(\"$array[$i]\")'>$array[$i]</a></li>";
        }

        echo "</ul> </div>";

    }

    static function generateHospitalTable($hospitalArray){
        echo "
        <table>
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
        ";
        foreach($hospitalArray as $hospital) {
            //TODO: Link dynamically!
            echo "<tr><td><a class='nostyle' href='hospital-details.php'>" . $hospital->getName() . "</a></td></tr>";
        }
        echo "</table>";
    }

    static function generateSuggestionsTable($suggestionList){
        echo "<form action='hospital-list.php' method='post'>
        <table>
          <thead>
            <tr>
              <th>Select</th>
              <th>Name</th>
            </tr>
          </thead>
        ";
        foreach($suggestionList as $suggestion) {
            echo "<tr>
              <td><input type='checkbox' checked name='" . $suggestion->getId() . "' /></td>
              <td>" . $suggestion->getName() . "</td>
            </tr>";
        }
        echo "</table>
          <input type='submit' value='Check Hospitals!' />
        </form>";
    }

}