/**
 * Created by cybered on 9/5/15.
 */

var fieldNum = 0;
var fieldValue = document.getElementById("defaultfield").innerHTML;

function addOption(str){
    var field = document.getElementById("autofield" + fieldNum);
    field.value = str;
    field.name = "conditions[" + fieldNum +"]";
    finalize(field);
}
function finalize(field){
    $(field).prop("readonly","true");
    $("#ajax").html("");
    addNewField();
}
function addNewField(){
    fieldNum++;
    $("#generated").append( fieldValue );
    document.getElementById("new").id = "autofield" + fieldNum;
}

function showResult(str) {
    if (str.length==0) {
        document.getElementById("ajax").innerHTML="";
        document.getElementById("ajax").style.border="0px";
        return;
    }
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            document.getElementById("ajax").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","/res/scripts/condition-search.php?q="+str,true);
    xmlhttp.send();
}