<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 7:58 PM
 */

class Hospital {
    private $name = null;
    private $phone = null;
    private $address = null;
    private $services = [];
    private $type = null;

    public function __construct($name, $phone, $address){
        $this->setName($name);
        $this->phone = $phone;
        $this->address = $address;
    }

    //getters & setters
    public function setName($name){
        $this->name = $name;
    }
    public function setPhone($phone){
        $this->phone = $phone;
    }
    public function setAddress($address){
        $this->address = $address;
    }
    public function setServices($services){
        $this->services = $services;
    }
    public function setType($type){
        //TODO: Validate!
        $this->type = $type;
    }

    public function getName(){
        return $this->name;
    }
    public function getPhone(){
        return $this->phone;
    }
    public function getAddress(){
        return $this->address;
    }
    public function getType(){
        return $this->type;
    }

    //The DB has all our data in CSV format. We will need to parse it.
    public function setServicesFromCSV($servicesCSV){
        $this->setServices(str_getcsv($servicesCSV));
    }

}