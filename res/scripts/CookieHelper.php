<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 6:33 PM
 */

require_once "TokenGenerator.php";
require_once "User.php";

class CookieHelper {
    static $VALID_TIME = 2592000; //86400 = 1 day, current value is 1 month

    static function registerCookie($key, $value){
        setcookie($key, $value, time() + self::$VALID_TIME, "/");
    }

    static function getToken(){
        if(isset($_COOKIE[TokenGenerator::$COOKIE_KEY])){
            return $_COOKIE[TokenGenerator::$COOKIE_KEY];
        }else{
            return null;
        }
    }

    static function getUser(){
        if(isset($_COOKIE[User::$COOKIE_KEY])){
            return $_COOKIE[User::$COOKIE_KEY];
        }else{
            return null;
        }
    }

    static function destroy()
    {
        // unset cookies
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach ($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000);
                setcookie($name, '', time() - 1000, '/');
            }
        }
    }
}