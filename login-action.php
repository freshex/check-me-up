<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 6:15 PM
 */

require_once "res/scripts/DbHelper.php";
require_once "res/scripts/TokenGenerator.php";
require_once "res/scripts/CookieHelper.php";
require_once "res/scripts/CryptHelper.php";
 
$email = $_POST['email'];
$password = $_POST['password'];

$loginSuccess = CryptHelper::attemptLogin($email, $password);

//Ensure the header include is AFTER the above code
require "res/header.php";

if($loginSuccess){
    echo "Welcome back!";
    echo "<script type=\"text/javascript\"> window.location = \"/index2.php\";</script>";
}else{
    echo "Invalid username or password.";
}

require "res/footer.php";