<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 10:04 PM
 */

require_once "res/scripts/DbHelper.php";
require_once "res/scripts/UI.php";

//TODO: STOP THIS HARDCODING...
$services = ["allergy","surgery"];

$dbHelper = new DbHelper();
$hospitalList = $dbHelper->searchHospitalByServices($services);

require "res/header.php";
?>

     <div class="nav">
       <ul>
         <li><a href="mycheckups.php">Back</a></li>
       </ul>
     </div>

<div class="center">
<?php UI::generateHospitalTable($hospitalList); ?>
</div>

<?php require "res/footer.php"; ?>