<?php require 'res/header.php'; ?>
<!-- <title>Check Me Up - User Information</title> -->

     <div class="nav">
       <ul>
         <li><a href="index2.php">Back</a></li>
       </ul>
     </div>


<h1>User Information</h1>
<p>Please fill the form below with as mush detail as possible, so we can accurately display suggestions for you.</p>

<form action="submit-preferences.php" method="post">

    Phone number <br>
    <input type="tel" name="phone">
    <br><br>

    Date of birth*<br>
    <input type="date" name="dob">
    <br><br>

    Gender*<br>
    <input type="radio" name="sex" value="male" checked> Male
    <br>
    <input type="radio" name="sex" value="female"> Female
    <br>
    <input type="radio" name="sex" value="other"> Other
    <br><br>

    Which insurance company do you use?
    <input type="text" name="insurance">
    <br>
    Which plan do you use?
    <input type="text" name="plan">
    <br><br>

    Do you have any known pre-existing health conditions? <br>
    <br>
    <div id="generated">
        <input id="autofield0" type="text" name="conditions[]" onkeyup="showResult(this.value)"/><br />
    </div>
    <div id="ajax" class="center"></div>
    <br><br>

    <input type="submit" value="Update my info!">
</form>

<!-- Blank field - used for copying -->
<div id="defaultfield" style="display: none"><input id="new" type="text" name="" onkeyup="showResult(this.value)"/><br /></div>

<!-- AJAX & Popup code -->
<script src="/res/scripts/autocomplete-popup.js"></script>

<?php require 'res/footer.php'; ?>
