<?php require 'res/header.php'; ?>

<head>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 75%;
	width: 50%
        margin: 0;
        padding: 0;
      }
      #map {
        height: 50%;
	width: 50%
      }
    </style>
  </head>

<header>
     <div class="nav">
       <ul>
         <li><a href="hospital-list.php">Back</a></li>
       </ul>
     </div>
<h1>
Hospital of the University of Pennsylvania
</h1>

<p> 
Private Hospital
</p>


<p> 
<a class='nostyle' href="tel:2156624000"> Call! (215) 662-4000 </a>

</p>

 
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3058.6218713003573!2d-75.1931279!3d39.949847!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c6c65bafb70b4f%3A0x5be8cf0e66e97fba!2sHospital+of+the+University+of+Pennsylvania!5e0!3m2!1sen!2sus!4v1441538399813" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


<p> 
Hospital services: <br>
Allergy & Immunology, Anesthesia, Animal-Assisted Therapy, Asthma, Bariatric Surgery, Blood Donation Center, Cancer, Cardiology & Cardiac Surgery, Dermatology, Diabetes, Epilepsy, Emergency Medicine, Endocrine & Oncologic Surgery, Endocrinology, Fertility Care, Gastroenterology, Gastrointestinal Surgery, Genetics, Geriatric Medicine, Gynecology, Hematology-Oncology, Home Infusion Therapy, Hyperbaric Medicine, Infectious Diseases,  Internal Medicine, Interventional Radiolog, Laboratory Medicine, Medicine, Neurology, Neurosurgery, Neurosciences, Ob/Gyn Care, Occupational Medicine, Ophthalmology, Oral & Maxillofacial Surgery, Orthopaedics, Otorhinolaryngology: Head & Neck Surgery, Pathology and Laboratory Medicine, Pediatrics, Physical Medicine and Rehabilitation, Plastic Surgery, Primary Care, Psychiatry, Pulmonary Medicine, Radiation Oncology, Radiology, Renal Disease, Rheumatology, Sleep Medicine, Surgery, Thoracic Surgery, Transplant, Traumatology & Surgical Critical Care, Travel Medicine, Urology, Vascular Medicine & Surgery, Women's Health
</p>
<?php require 'res/footer.php'; ?>