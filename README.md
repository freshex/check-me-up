## Inspiration

Most people do not know the check-ups they need and where they can have them. Ones who know the need search for check-ups by contacting hospitals themselves. Overall, many people skip necessary check-ups and miss early diagnosis chance. Additionally, process of getting an appointment from an hospital that has a suitable check-up program for the individual is elusive and tiresome. People need a service that can guide them through, by telling them what check-ups they need, where they can have them with regard to their insurance/healthcare plans and location, and leading to an appointment.

## What it does

The project integrates different parts of check-up process in an app to lead the patients. First, the app will present a list of check-ups based on an acknowledged source such as WHO and CDC, and users data, such as gender, age, and health info. User will select check-up programs, and the app will display a list of hospitals that have the selected check-ups, arranged according to user’s preferences such as distance to hospital and costs depending on healthcare and insurance plans of the user covered by hospitals. The user will click on a single button to contact a selected hospital to make an appointment.

## How I built it

We used Microsoft Azure as a web app host, and MySql as a database. Then, we designed an app based on web, using web development languages such as PHP, CSS, HTML and JavaScript. Then, we generated mobile apps that can support iOS, Android, and Windows using Manifoldjs. We utilized features like Google Maps and calling intents. We also implemented an authentication system for secure storage and user information.

## Challenges I ran into

Azure initially provided a depricated PHP version by default, which we had to troubleshoot excessively to figure out. Designing the web based app to suit the mobile versions was also a challenge. Most of us had to hack using entirely new environments and platforms such as Photoshop, Azure and mobile web designs, which required intense research and practice. We had to solve complicated algorithm designs to provide accurate and customized suggestions.

## Accomplishments that I'm proud of

We generated an app that can work on virtually everything: iOs, Windows, Android, Web... Also, we believe that we tackled a problem which has been obscure for everyone.

## What I learned

We learned teamwork (Hurray for git!), new languages, platformi and environments, time management and communication skills.

## What's next for Check Me Up

The version we provide now has the fundamental features, yet has endless opportunity for the future. Additional features such as periodic check-up warnings and guidance to hospitals in case of minor injuries and health issues can be added. Upcoming partnerships with health organizations and social institutions will impact the masses. 