<?php
require 'res/header.php';
require_once "res/scripts/DbHelper.php";
require_once "res/scripts/UI.php";
?>

     <div class="nav">
       <ul>
         <li><a href="index2.php">Back</a></li>
       </ul>
     </div>
<h1>
Here are the suggested check ups!
</h1>

<div class="center">

<?php

$dbHelper = new DbHelper();
$sugList = $dbHelper->getSuggestionsByUser($dbHelper->selectCurrentUser());
UI::generateSuggestionsTable($sugList);

?>

</div>

<?php require 'res/footer.php'; ?>