<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 4:55 PM
 */

require "res/scripts/User.php";

$name = $_POST['fullname'];
$password = $_POST['password1'];
$passwordConfirm = $_POST['password2'];
$email = $_POST['email'];

if($password == $passwordConfirm) {
    $user = new User($email, null, $name);
    $user->generatePassword($password);
    $success = $user->register();
}

if($success) {
    //automatically login
    $loginSuccess = CryptHelper::attemptLogin($email, $password);
}

require "res/header.php";

if($success && $loginSuccess){
    echo "<p>Hoorah! Now you can personalize your account.";
    echo "<script type=\"text/javascript\"> window.location = \"/user-form.php\";</script>";
}else{
    echo "<p>We could not create your account. Please verify the passwords match.";
}

require "res/footer.php";
