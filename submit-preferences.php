<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 9/5/15
 * Time: 11:28 PM
 */

require_once "res/scripts/DbHelper.php";

$phone = $_POST['phone'];
$dob = $_POST['dob'];
$sex = $_POST['sex'];
$insurance = $_POST['insurance'];
$plan = $_POST['plan'];
$conditionsArray = $_POST['conditions'];

$dbHelper = new DbHelper();
$user = $dbHelper->selectCurrentUser();

$user->setPhone($phone);
$user->setDob($dob);
$user->setSex($sex);
$user->setInsurance($insurance);
$user->setPlan($plan);
$user->setConditionsArray($conditionsArray);

$user->update();

echo "<script type=\"text/javascript\"> window.location = \"/preferences.php\";</script>";