<?php
require 'res/header.php';
require_once 'res/scripts/CookieHelper.php';

if(isset($_GET['logoff']) && $_GET['logoff'] == 1) {
    CookieHelper::destroy();
}else{
    if(CookieHelper::getUser() != null){
        echo '<script type="text/javascript"> window.location = "index2.php" </script>';
    }
}



?>

<h1>Welcome to Check Me Up!</h1>
 
<p>Create a Check Me Up account to get information on hospital and check-up services.</p>

 <a class="button fixedwidth" href="/login.php">Sign in</a> 
 <br>
 <a class="button fixedwidth" href="/user-registration.php">Create New Account</a>
 <br>
 <a class="button facebook fixedwidth">Connect with Facebook</a>
 <br> 
 <a class="button twitter fixedwidth">Connect with Twitter</a>
 
 <?php require 'res/footer.php'; ?>